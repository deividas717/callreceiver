package receiver.call.com.callreceiver.storage

import android.content.Context
import android.content.SharedPreferences
import android.telephony.TelephonyManager
import receiver.call.com.callreceiver.storage.dto.User

object SharedPreferencesManager {
    private const val NAME = "CallReceiver"
    private const val MODE = Context.MODE_PRIVATE
    private lateinit var preferences: SharedPreferences

    private const val userPhoneNumber = "userPhoneNumber"
    private const val wasAnswered = "wasAnswered"
    private const val allowToSendTAG = "allowToSendTAG"

    private inline fun SharedPreferences.edit(operation: (SharedPreferences.Editor) -> Unit) {
        val editor = edit()
        operation(editor)
        editor.apply()
    }

    fun init(context: Context) {
        preferences = context.getSharedPreferences(NAME, MODE)
    }

    var user: User? = null
        set(value) = preferences.edit {
            if (value != null) {
                it.apply {
                    putString(userPhoneNumber, value.userPhoneNumber)
                }
            } else {
                it.apply {
                    remove(userPhoneNumber)
                }
            }
            field = value
        }
        get() {
            if (field != null) return field

            val userPhoneNum = preferences.getString(userPhoneNumber, null) ?: return null

            return User(userPhoneNum)
        }

    var wasCallAnswered: Boolean = false
        set(value) = preferences.edit {
            it.putBoolean(wasAnswered, value).apply()
            field = value
        }
        get () {
            return preferences.getBoolean(wasAnswered, false)
        }

    var callState: Int = TelephonyManager.CALL_STATE_IDLE
        set(value) = preferences.edit {
            it.putInt(allowToSendTAG, value).apply()
            field = value
        }
        get () {
            return preferences.getInt(allowToSendTAG, TelephonyManager.CALL_STATE_IDLE)
        }
}