package receiver.call.com.callreceiver.receiver

import android.content.Context
import receiver.call.com.callreceiver.ForegrounCustomService

class CallReceiver: PhoneCallReceiver() {

    override fun onIncomingCallAnswered(context: Context, number: String) {
        context.startService(ForegrounCustomService.createIntent(context, number, START))
    }

    override fun onIncomingCallEnded(context: Context, number: String) {
        context.startService(ForegrounCustomService.createIntent(context, number, STOP))
    }

    companion object {
        const val START = "start"
        const val STOP = "stop"
    }
}