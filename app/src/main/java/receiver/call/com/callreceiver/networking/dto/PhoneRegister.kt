package receiver.call.com.callreceiver.networking.dto

data class PhoneRegister(val u: String, val n: String, val d: String)