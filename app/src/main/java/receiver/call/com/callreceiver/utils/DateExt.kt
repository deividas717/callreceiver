package receiver.call.com.callreceiver.utils

import java.text.SimpleDateFormat
import java.util.*

fun Date.getDateAndTimeAsFormattedString(): String {
    val simpleDateFormat = SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.ENGLISH)
    return simpleDateFormat.format(this)
}