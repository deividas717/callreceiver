package receiver.call.com.callreceiver

import android.app.Application
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import receiver.call.com.callreceiver.networking.ApiService
import receiver.call.com.callreceiver.utils.Constants
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import com.google.gson.GsonBuilder
import receiver.call.com.callreceiver.storage.SharedPreferencesManager
import java.util.concurrent.TimeUnit

class CallReceiverApp: Application() {
    private var service: ApiService? = null

    override fun onCreate() {
        super.onCreate()

        SharedPreferencesManager.init(applicationContext)
    }

    fun getRetrofit() : ApiService? {
        if (service == null) {
            val client = OkHttpClient.Builder()
                .connectTimeout(100, TimeUnit.SECONDS)
                .readTimeout(100,TimeUnit.SECONDS)
                .addInterceptor(HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BODY))
                .build()

            val gson = GsonBuilder()
                .setDateFormat("yyyy-MM-dd HH:mm:ss")
                .create()

            val retrofit = Retrofit.Builder()
                .baseUrl(Constants.serverAddress)
                .client(client)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build()
            service = retrofit.create(ApiService::class.java)
        }
        return service
    }
}