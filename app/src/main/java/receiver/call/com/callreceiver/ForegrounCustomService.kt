package receiver.call.com.callreceiver

import android.app.IntentService
import android.content.Intent
import android.content.Context
import android.support.v4.app.NotificationCompat
import okhttp3.MultipartBody
import receiver.call.com.callreceiver.networking.dto.PhoneRegister
import receiver.call.com.callreceiver.storage.SharedPreferencesManager
import receiver.call.com.callreceiver.storage.dto.User
import receiver.call.com.callreceiver.utils.getDateAndTimeAsFormattedString
import java.util.*

class ForegrounCustomService : IntentService("ForegrounCustomService") {


    override fun onCreate() {
        super.onCreate()

        val notification = NotificationCompat.Builder(this)
            .setContentTitle("")
            .setContentText("").build()

        val uniqueInt = (System.currentTimeMillis() and 0xfffffff).toInt()
        startForeground(uniqueInt, notification)
    }

    override fun onHandleIntent(intent: Intent?) {
        if (intent != null) {
            val user = SharedPreferencesManager.user ?: return
            val tag = intent.getStringExtra("tag")
            val number = intent.getStringExtra("number")

            val body = createRequestBody(user, number, tag)
            sendDataToServer(applicationContext, body)
        }
    }

    private fun createRequestBody(user: User, number: String, type: String) : MultipartBody {
        val date = Date().getDateAndTimeAsFormattedString()
        val body = PhoneRegister(u = user.userPhoneNumber, n = number, d = date)
        return MultipartBody.Builder()
            .setType(MultipartBody.FORM)
            .addFormDataPart("u", body.u)
            .addFormDataPart("n", body.n)
            .addFormDataPart("d", body.d)
            .addFormDataPart("a", type)
            .build()
    }

    private fun sendDataToServer(context: Context, body: MultipartBody, retry: Boolean = true) {
        val apiService = (context as CallReceiverApp).getRetrofit()

        val request = apiService?.registerPhoneNumber(body)?.execute()
        if (request != null && !request.isSuccessful && retry) {
            sendDataToServer(context, body, false)
        }
    }

    @Override
    override fun onDestroy() {
        stopForeground(true);
    }
    companion object {
        fun createIntent(context: Context, number: String, tag: String) =
            Intent(context, ForegrounCustomService::class.java).putExtra("tag", tag).putExtra("number", number)!!
    }
}
