package receiver.call.com.callreceiver.receiver

import android.annotation.SuppressLint
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.telephony.TelephonyManager

abstract class PhoneCallReceiver: BroadcastReceiver() {

    @SuppressLint("MissingPermission")
    override fun onReceive(context: Context?, intent: Intent?) {
        if (intent == null || context == null) return

        if (intent.action != Intent.ACTION_NEW_OUTGOING_CALL) {
            val stateString = intent.extras?.getString(TelephonyManager.EXTRA_STATE) ?: return
            val number = intent.extras?.getString(TelephonyManager.EXTRA_INCOMING_NUMBER) ?: return
            val state = when (stateString) {
                TelephonyManager.EXTRA_STATE_IDLE -> TelephonyManager.CALL_STATE_IDLE
                TelephonyManager.EXTRA_STATE_OFFHOOK -> TelephonyManager.CALL_STATE_OFFHOOK
                TelephonyManager.EXTRA_STATE_RINGING -> TelephonyManager.CALL_STATE_RINGING
                else -> TelephonyManager.CALL_STATE_IDLE
            }
            onCallStateChanged(context, state, number)
        }
    }

    private fun onCallStateChanged(context: Context, state: Int, number: String) {
        when (state) {
            TelephonyManager.CALL_STATE_RINGING -> isIncommingCall = true
            TelephonyManager.CALL_STATE_OFFHOOK -> {
                isIncommingCall = lastState == TelephonyManager.CALL_STATE_RINGING
                if (isIncommingCall) {
                    onIncomingCallAnswered(context, number)
                }
            }
            TelephonyManager.CALL_STATE_IDLE -> {
                if (isIncommingCall) {
                    onIncomingCallEnded(context, number)
                }
            }
        }
        lastState = state
    }

    protected abstract fun onIncomingCallAnswered(context: Context, number: String)
    protected abstract fun onIncomingCallEnded(context: Context, number: String)

    companion object {
        private var isIncommingCall = false
        private var lastState = TelephonyManager.CALL_STATE_IDLE
    }
}