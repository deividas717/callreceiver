package receiver.call.com.callreceiver.networking

import okhttp3.RequestBody
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.http.Body
import retrofit2.http.POST

interface ApiService {
    @POST("CallImport")
    fun registerPhoneNumber(@Body phoneBody: RequestBody) : Call<ResponseBody>
}