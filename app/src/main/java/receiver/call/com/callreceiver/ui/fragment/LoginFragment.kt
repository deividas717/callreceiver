package receiver.call.com.callreceiver.ui.fragment

import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.content.ContextCompat
import android.telephony.PhoneNumberUtils
import android.text.TextUtils
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import kotlinx.android.synthetic.main.fragment_login.*
import kotlinx.android.synthetic.main.fragment_login.view.*
import receiver.call.com.callreceiver.ForegrounCustomService

import receiver.call.com.callreceiver.R
import receiver.call.com.callreceiver.storage.SharedPreferencesManager
import receiver.call.com.callreceiver.storage.dto.User
import android.content.Intent



class LoginFragment : Fragment() {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_login, container, false)
        if (SharedPreferencesManager.user != null) view.viewSwitcher.showNext()

        view.loginBtn.setOnClickListener {
            val userPhoneNumber = phoneEditText.text.toString()
            if (TextUtils.isEmpty(userPhoneNumber) || !PhoneNumberUtils.isGlobalPhoneNumber(userPhoneNumber)) {
                Toast.makeText(activity!!.applicationContext, "Telefono numeris neteisingas!",  Toast.LENGTH_SHORT).show()
                return@setOnClickListener
            }
            SharedPreferencesManager.user = User(userPhoneNumber)
            view.viewSwitcher.showNext()
        }

        view.logoutBtn.setOnClickListener {
            SharedPreferencesManager.user = null
            SharedPreferencesManager.wasCallAnswered = false
            view.viewSwitcher.showPrevious()
        }
        return view
    }

    companion object {
        fun newInstance() = LoginFragment()
    }
}
