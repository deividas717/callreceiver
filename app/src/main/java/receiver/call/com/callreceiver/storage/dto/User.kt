package receiver.call.com.callreceiver.storage.dto

data class User(val userPhoneNumber: String)